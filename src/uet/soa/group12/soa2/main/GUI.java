package uet.soa.group12.soa2.main;

import java.io.File;
import java.security.Key;
import java.security.KeyPair;
import java.util.Scanner;

import org.w3c.dom.Document;

import uet.soa.group12.soa2.logic.MyDecryption;
import uet.soa.group12.soa2.logic.MyEncryption;
import uet.soa.group12.soa2.logic.PayloadEncryption;
import uet.soa.group12.soa2.logic.XmlUtil;

public class GUI {
	public static void main(String[] args) {

		System.out.println("-------------------------------------");
		System.out.println("XML Encryption/Decryption program");
		System.out.println("=>> Generating Keypair....");
		KeyPair keyPair = MyEncryption.getKeyPair();
		System.out.println("   OK : Private key: "
				+ keyPair.getPrivate().getEncoded());
		System.out.println("   OK : Public key:  "
				+ keyPair.getPublic().toString().getBytes());
		System.out.println("-------------------------------------");

		System.out.println(" Encrypting file..........");
		Scanner in = new Scanner(System.in);

		System.out.print(" =>> Please enter fileName to encrypt: ");
		String fileName = in.next();
		System.out.println("   OK : File name is " + fileName);

		System.out.print(" =>> Please spercify xpath to xml element: ");
		String xpath = in.next();
		System.out.println("   OK : xPath is " + xpath);

		// Document encryptedDoc = MyEncryption.encryptXML("payment.xml",
		// "/PurchaseOrderRequest/Payment",keyPair);
		Document encryptedDoc = MyEncryption.encryptXML(fileName, xpath,
				keyPair);
		System.out.println("-------------------------------------");
		System.out
				.print("=>>  Enter file name to save the encrypted xml file: ");
		String saveFileName = in.next();
		XmlUtil.XmltoFile(encryptedDoc, new File(saveFileName));
		System.out.println("Saved encrypted xml to file: " + saveFileName);
		
		System.out.println("-------------------------------------");
		System.out.println("-------------------------------------");
		System.out.println(" Decrypting file..........");
		System.out.print(" =>> Please enter fileName to decrypt: ");
		fileName = in.next();
		System.out.println("   OK : File name is " + fileName);
		Document decryptedDoc = MyDecryption.decryptXML(keyPair, fileName);
		XmlUtil.XmlOnStdOut(decryptedDoc);
		System.out.println("-------------------------------------");
		System.out
				.print("=>>  Enter file name to save the decrypted xml file: ");
		saveFileName = in.next();
		XmlUtil.XmltoFile(decryptedDoc, new File(saveFileName));
		System.out.println("Saved decrypted xml to file: " + saveFileName);
	}
}
