package uet.soa.group12.soa2;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import com.verisign.xmlenc.AlgorithmType;
import com.verisign.xmlsig.KeyInfo;

import javax.crypto.KeyGenerator;

import org.w3c.dom.Document;

public class MyEncryption {
	private static void encryptXML(String fileName, String xpath) {

		Document doc = XmlUtil.getDocument(fileName);
		Key ky = getKey();
		KeyPair keyPair = getKeyPair();
		Key publicKey = keyPair.getPublic();
		KeyInfo keyInfo = new KeyInfo();
		doc = PayloadEncryption.encryptDataWithKey(xpath, doc, ky,
				AlgorithmType.TRIPLEDES, publicKey, AlgorithmType.RSA1_5,
				keyInfo);
		System.out.println("Decrypting with key");
		Key privateKey = keyPair.getPrivate();
		System.out.println("Private key alogorithm "
				+ privateKey.getAlgorithm());
		xpath = "//xenc:EncryptedData";
		doc = PayloadEncryption.decryptDataAndKey(xpath, doc, privateKey);
	}

	public static KeyPair getKeyPair() {

		KeyPairGenerator pairgen = null;
		try {
			pairgen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			System.out
					.println("There is no such algorithm for generating the keys");
			e.printStackTrace();
		}
		SecureRandom random = new SecureRandom();
		int KEYSIZE = 1024;
		pairgen.initialize(KEYSIZE, random);
		KeyPair keyPair = pairgen.generateKeyPair();

		return keyPair;

	}

	static Key getKey() {
		Key ky = null;
		KeyGenerator kg = null;
		try {
			kg = KeyGenerator.getInstance("DESede");
		} catch (NoSuchAlgorithmException e) {
			System.out
					.println("There is no such algorithm for generating the shared secret");
			e.printStackTrace();
		}
		ky = kg.generateKey();
		return ky;
	}

	public static void main(String arg[]) {
		encryptXML("payment.xml", "/PurchaseOrderRequest/Payment");
	}

}
