/*
 * Created on Mar 25, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package uet.soa.group12.soa2;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;

import org.w3c.dom.Document;

import com.verisign.xmlsig.Signer;
import com.verisign.xmlsig.Verifier;
import com.verisign.xpath.XPath;
import com.verisign.xpath.XPathException;

/**
 * @author Manish Verma
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PayloadSignature {
	public Document sign(
		String xPath,
		Document doc,
		PrivateKey privateKey,
		PublicKey verificationKey) {
		Document d = null;
		Signer signer = null;
		try {
			signer = new Signer(doc, privateKey, verificationKey);
			
			XPath location1 = new XPath("/PurchaseOrderRequest/Payment/CreditCard");
			signer.addReference(location1);
			
			XPath location2 = new XPath("/PurchaseOrderRequest/Payment/PurchaseAmount");
			signer.addReference(location2);
			
		} catch (InvalidKeyException e) {
			// XXX Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// XXX Auto-generated catch block
			e.printStackTrace();
		}

		try {

		d = signer.sign();
		} catch (InvalidKeyException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		} catch (SignatureException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		} catch (XPathException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		}
		return d;

	}

	public boolean verify(String Xpath, Document doc) {
		String ns[] = { "ds", "http://www.w3.org/2000/09/xmldsig#" };
		//XPath signatureLocation = new XPath("//ds:Signature", ns);
		XPath signatureLocation = new XPath(Xpath, ns);
		Verifier verifier = null;
		try {
			verifier = new Verifier(doc, signatureLocation);
			try {
				System.out.println("Verifying key algorithm " + verifier.getVerifyingKey().getAlgorithm());
			} catch (InvalidKeyException e2) {
				// XXX Auto-generated catch block
				e2.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e) {
			// XXX Auto-generated catch block
			e.printStackTrace();
		} catch (XPathException e) {
			// XXX Auto-generated catch block
			e.printStackTrace();
		}
		try {
			return verifier.verify();
		} catch (InvalidKeyException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		} catch (SignatureException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		} catch (XPathException e1) {
			// XXX Auto-generated catch block
			e1.printStackTrace();
		}
		return false;

	}

}
