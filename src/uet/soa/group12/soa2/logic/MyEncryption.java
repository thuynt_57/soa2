package uet.soa.group12.soa2.logic;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import com.verisign.xmlenc.AlgorithmType;
import com.verisign.xmlsig.KeyInfo;

import javax.crypto.KeyGenerator;

import org.w3c.dom.Document;

public class MyEncryption {
	public static Document encryptXML(String fileName, String xpath,KeyPair keyPair) {

		Document doc = XmlUtil.getDocument(fileName);
		Key ky = getKey();		
		Key publicKey = keyPair.getPublic();
		KeyInfo keyInfo = new KeyInfo();
		doc = PayloadEncryption.encryptDataWithKey(xpath, doc, ky,
				AlgorithmType.TRIPLEDES, publicKey, AlgorithmType.RSA1_5,
				keyInfo);
		
		return doc;
	}

	public static KeyPair getKeyPair() {

		KeyPairGenerator pairgen = null;
		try {
			pairgen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			System.out
					.println("There is no such algorithm for generating the keys");
			e.printStackTrace();
		}
		SecureRandom random = new SecureRandom();
		int KEYSIZE = 1024;
		pairgen.initialize(KEYSIZE, random);
		KeyPair keyPair = pairgen.generateKeyPair();

		return keyPair;

	}

	public static Key getKey() {
		Key ky = null;
		KeyGenerator kg = null;
		try {
			kg = KeyGenerator.getInstance("DESede");
		} catch (NoSuchAlgorithmException e) {
			System.out
					.println("There is no such algorithm for generating the shared secret");
			e.printStackTrace();
		}
		ky = kg.generateKey();
		return ky;
	}
}
