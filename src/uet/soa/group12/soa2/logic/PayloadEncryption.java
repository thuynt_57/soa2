package uet.soa.group12.soa2.logic;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import org.w3c.dom.Document;

import com.verisign.xmlenc.AlgorithmType;
import com.verisign.xmlenc.Decryptor;
import com.verisign.xmlenc.Encryptor;
import com.verisign.xmlsig.KeyInfo;
import com.verisign.xpath.XPath;
import com.verisign.xpath.XPathException;

public class PayloadEncryption {

	public static Document decryptData(String xPath, Document doc, Key ky) {
		String[] ns = { "xenc", "http://www.w3.org/2001/04/xmlenc#" };
		XPath xpath = new XPath(xPath, ns);
		try {
			Decryptor decrypt = new Decryptor(doc, ky, xpath);
			decrypt.decryptInPlace();
//			XmlUtil.XmlOnStdOut(doc);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return doc;
	}

	public static Document encryptData(String xPath, Document doc, Key ky) {
		try {
			Encryptor enc2 = new Encryptor(doc, ky, AlgorithmType.TRIPLEDES);
			XPath xpath = new XPath(xPath);
			enc2.encryptInPlace(xpath);
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return doc;
	}

	public static Document encryptDataWithKey(String xPath, Document doc,
			Key dataEncryptionKey, AlgorithmType dataEncryptionAlgoType,
			Key keyEncryptionKey, AlgorithmType keyEncryptionAlgoType,
			KeyInfo keyInfo) {
		try {
			Encryptor enc = new Encryptor(doc, dataEncryptionKey,
					dataEncryptionAlgoType, keyEncryptionKey,
					keyEncryptionAlgoType, keyInfo);
			XPath xpath = new XPath(xPath);
			try {
				enc.encryptInPlace(xpath);
			} catch (XPathException e1) {
				// XXX Auto-generated catch block
				e1.printStackTrace();
			}							
		} catch (IllegalArgumentException e) {
			// XXX Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// XXX Auto-generated catch block
			e.printStackTrace();
		}

		return doc;
	}

	public static Document decryptDataAndKey(String xPath, Document doc,
			Key privateKey) {

		String[] ns = { "xenc", "http://www.w3.org/2001/04/xmlenc#" };
		XPath xpath = new XPath(xPath, ns);
		Decryptor decrypt = null;
		try {
			decrypt = new Decryptor(doc, privateKey, xpath);
		} catch (IllegalArgumentException e) {
			System.out.println("Illegal argument exception");
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.out.println("The specifed algorithm is not supported");
			e.printStackTrace();
		}
		try {
			decrypt.decryptInPlace();
		} catch (IllegalArgumentException e1) {
			System.out.println("Illegal argument exception");
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			System.out.println("The specifed algorithm is not supported");
			e1.printStackTrace();
		} catch (XPathException e1) {
			System.out.println("Xpath is invalid");
			e1.printStackTrace();
		}
//		XmlUtil.XmlOnStdOut(doc);
		return doc;
	}

}