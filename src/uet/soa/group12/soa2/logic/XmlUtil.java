package uet.soa.group12.soa2.logic;

/**
 * @author Manish Verma
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.*;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class XmlUtil {

	/**
	  * Returns the DOM document for the XML document whose file name is passed in
	  * as the argument
	  *
	  * @param fileName - XML whose DOM document is to be created
	  *
	  */
	public static Document getDocument(String fileName) {
		Document doc = null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		File f = new File(fileName);
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.out.println("Parse configuration exception");
			e.printStackTrace();
		}
		try {
			doc = builder.parse(f);
		} catch (SAXException e1) {
			System.out.println("SAX exception");
			e1.printStackTrace();
		} catch (IOException e1) {
			System.out.println("IO Exception");
			e1.printStackTrace();
		}

		return doc;
	}

	/**
	 * Returns the root element of the DOm document
	 *
	 * @param domDocument DOM document whose root element is to be found
	 */
	public static Node getRootElement(Document domDocument) {
		return domDocument.getDocumentElement();
	}

	/**
	 * Returns the name of the element after purging the namespace from it
	 *
	 * @param value Fully qualified name of the element from which
	 * name space is to be purged.
	 */
	public static String purgeNamespace(String value) {
		String nameSpaceSeperator = ":";
		if (value != null) {
			int seperatorIndex = value.lastIndexOf(nameSpaceSeperator);
			if (seperatorIndex != -1)
				return value.substring(
					value.lastIndexOf(nameSpaceSeperator) + 1);
			else
				return value;
		} else
			return value;
	}

	public static void XmlOnStdOut(Document doc) {

		try {

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(System.out);			
			transformer.transform(source, result);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void XmltoFile(Document doc,File file) {

		try {

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);			
			transformer.transform(source, result);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	

}
