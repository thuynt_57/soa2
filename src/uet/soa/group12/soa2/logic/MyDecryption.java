package uet.soa.group12.soa2.logic;

import java.security.Key;
import java.security.KeyPair;

import org.w3c.dom.Document;

import com.verisign.xmlenc.Decryptor;
import com.verisign.xpath.XPath;

public class MyDecryption {	
	public static Document decryptXML(KeyPair keyPair,String xmlFile){
		Document doc = XmlUtil.getDocument(xmlFile);
		Key privateKey = keyPair.getPrivate();
		String xPath = "//xenc:EncryptedData";
		return PayloadEncryption.decryptDataAndKey(xPath, doc, privateKey);
	}	
}
