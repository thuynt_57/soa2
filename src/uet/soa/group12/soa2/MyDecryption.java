package uet.soa.group12.soa2;

import java.security.Key;
import java.security.KeyPair;

import org.w3c.dom.Document;

import com.verisign.xmlenc.Decryptor;
import com.verisign.xpath.XPath;

public class MyDecryption {
	private static String encryptedXmlFileName ="sample.xml";
	public static void decryptXML(KeyPair keyPair,String xmlFile){
		Document doc = XmlUtil.getDocument(encryptedXmlFileName);
		Key privateKey = keyPair.getPrivate();
		String xpath1 = "//xenc:EncryptedData";
		String[] ns = 
			{ "xenc", "http://www.w3.org/2001/04/xmlenc#" };
		XPath xpath = new XPath(xpath1, ns);
		Decryptor decrypt = null;
		try {
			decrypt = new Decryptor(doc, privateKey, xpath);
		} catch (Exception e) {
			System.out.println("Some exception");
			e.printStackTrace();
		} 
		// Step 5. Method decryptInPlace is called to decrypt the
		// encrypted contents of the XML
		try {
			decrypt.decryptInPlace();
		} catch (Exception e1) {
			System.out.println("Some exception");
			e1.printStackTrace();

		}
	}	
	public static void main(String[] args){
	}
}
